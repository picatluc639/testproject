<!DOCTYPE html>
<html>
<br>
<head>
    <title>THE FLORIST SHOP</title>
    <style>
body {
  background-image: url('https://image.freepik.com/free-photo/high-angle-pink-gifts-with-copy-space_23-2148465494.jpg');
  background-repeat: no-repeat;
  background-attachment: fixed;
  background-size: 100% 100%;
}
    </style>
    <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <div class="container text-center">
        <h1 style="color:dark; font-family:'Montserrat', sans-serif">THE FLORIST SHOP</h1>
    </div>
</head>

<body>
    <?php require_once 'process.php';?>
<div class="container">  
    <div class="panel-heading">
    <form method="get">
    <input type="search" name="s" class="form-control" style="margin-top:15px; margin-bottom:15px;" placeholder="Search for">
    </form>
    </div> 

    <form action="process.php" method="POST" style="border: 1px solid white; border-radius:10px; padding: 10px 10px 10px 10px">
        <div class="form-group">
        <label>Name</label>
        <input type="text" name="name" class='form-control' placeholder="Enter flower's name">
        </div>
        <div class="form-group">
        <label>Origin</label>
        <input type="text" name="origin" class='form-control'placeholder="Enter flower's origin (French, Hungary, etc)">
        </div>
        <div class="form-group">
        <label>Amount</label>
        <input type="number" name="amount" class='form-control'>
        </div>
        <div class="form-group">
        <label>Note</label>
        <input type="text" name="note" class='form-control'placeholder="Color, etc">
        </div>
        <div class="form-group text-center"> 
        <button type="submit" name="save" class="btn btn-light">Save</button>
        </div>
    </form>    
        
    <?php 
        $result= mysqli_query($mysqli,"SELECT id,name,origin,amount,note FROM data");         
    ?>
        <div class="row justify-content-center container">
            <table class="table" style="font-size:20px">
                <thead>
                    <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Origin</th>
                    <th>Amount</th>
                    <th>Note</th>
                    <th colspan="3" >Action</th>
                    </tr>
                </thead>
    <?php
        $mysqli = new mysqli('mysql','default','secret','default') or die(mysqli_error($mysqli));
 
        $result = mysqli_query($mysqli, 'select count(id) as total from data');
        $row = mysqli_fetch_assoc($result);
        $total_records = $row['total'];
      
        $current_page = isset($_GET['page']) ? $_GET['page'] : 1;
        $limit = 2;
           
        $total_page = ceil($total_records / $limit);
         
        if ($current_page > $total_page){
                $current_page = $total_page;
        }
            else if ($current_page < 1){
                $current_page = 1;
        } 
        $start = ($current_page - 1) * $limit; 
        $result=$mysqli->query("SELECT * FROM data LIMIT $start, $limit"); 
        

        if(isset($_GET['s']) && $_GET['s'] !='') {
            $result=$mysqli->query('SELECT * FROM data WHERE name like "%'.$_GET['s'].'%" OR origin like "%'.$_GET['s'].'%" OR amount like "%'.$_GET['s'].'%"');
        } else{
            $result=$mysqli->query("SELECT * FROM data LIMIT $start, $limit");
        }
            
        while($row=mysqli_fetch_array($result)) {?>   
                <tr>
                    <td><?php echo $row['id']?></td>
                    <td><?php echo $row['name']?></td>
                    <td><?php echo $row['origin']?></td>
                    <td><?php echo $row['amount']?></td>
                    <td><?php echo ""?></td>
                    <td>
                        <a href="edit.php?edit=<?php echo $row['id']; ?>"
                        class="btn btn-outline-dark">Edit</a>
                        <a href="index.php?delete=<?php echo $row['id']; ?>"
                        class="btn btn-outline-dark" onclick="return confirm('Are you sure?');">Delete</a>
                        <a href="detail.php?detail=<?php echo $row['id']; ?>"
                        class="btn btn-outline-dark" >Detail</a>
                    </td>
                </tr>
        <?php } ?>
            </table>
        <div class="form-group text-center"> 
        <div class="pagination">
    <?php
       if ($current_page > 1 && $total_page > 1){
        echo '<a href="index.php?page='.($current_page-1).'" class="btn btn-outline-light">Prev</a> ';
    }

    for ($i = 1; $i <= $total_page; $i++){
        if ($i == $current_page){
            echo '<span class="btn btn-dark">'.$i.'</span> ';
        }
        else{
            echo '<a href="index.php?page='.$i.'" class="btn btn-outline-light">'.$i.'</a> ';
        }
    }
    if ($current_page < $total_page && $total_page > 1){
        echo '<a href="index.php?page='.($current_page+1).'" class="btn btn-outline-light">Next</a> ';
    }     
    ?>
    </div>
    </div>      
</div>
<div>
    <a href="index.php" type="submit" name="home" >Home</a>
    </div>  
</body>
</html>
